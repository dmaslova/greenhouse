from __future__ import print_function, absolute_import

import tweepy
import pyupm_grove as grove
import time, sys, signal, atexit
import mraa
import pyupm_i2clcd as Lcd
import pyupm_buzzer as upmBuzzer
import pyupm_grove as grove
import pyupm_bmpx8x as upmBmp
import pyupm_ppd42ns as upmPpd
import threading
import datetime
from multiprocessing import Process, Value

import Servo

from flask import Flask
from flask import request
from flask import abort, jsonify, render_template
import threading

app = Flask(__name__)


@app.route("/")
def rootpage():
    return mainpage()

@app.route('/main', methods=['GET', 'POST'])
def mainpage():
    return render_template('main.html')


@app.route('/main.html', methods=['GET', 'POST'])
def mainpageP():
    return render_template('main.html')

@app.route('/index3', methods=['GET', 'POST'])
def index3():
    return render_template('index3.html')

@app.route('/index3.html', methods=['GET', 'POST'])
def index3P():
    return render_template('index3.html')

@app.route('/index2', methods=['GET', 'POST'])
def index2():
    return render_template('index2.html')

@app.route('/index2.html', methods=['GET', 'POST'])
def index2P():
    return render_template('index2.html')

@app.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@app.route('/index.html', methods=['GET', 'POST'])
def indexP():
    return render_template('index.html')

@app.route('/index4', methods=['GET', 'POST'])
def index4():
    return render_template('index4.html')

@app.route('/index4.html', methods=['GET', 'POST'])
def index4P():
    return render_template('index4.html')


@app.route('/settings.html', methods=['GET', 'POST'])
def settingsP():
    return render_template('settings.html')

@app.route('/settings', methods=['GET', 'POST'])
def settings():
    return render_template('settings.html')


@app.route('/temp', methods=['GET'])
def getTempratureA():
    temp = t.value
    return jsonify({'temperature':str(temp),
                    'success':True})

@app.route('/dust', methods=['GET'])
def getDustA():
    dust = d.value
    return jsonify({'dust':str(dust),
                    'success':True})

@app.route('/light', methods=['GET'])
def getLightA():
    light = l.value
    return jsonify({'light':str(light),
                    'success':True})

@app.route('/status', methods=['GET'])
def getStatusA():
    return jsonify({'res':'ok',
                    'playing':playing,
                    'red': red.read(),
                    'yellow':servo.read()/90,
                    'blue':blue.read(),
                    'sentense':msg,
                    'temperature':t.value,
                    'dust': d.value,
                    'light': l.value,
                    'success': True,
                    })

@app.route('/get_settings', methods=['GET'])
def getSettingsA():
    return jsonify({'daystart':stringifyHourPlusMinute(dayStartH, dayStartM),
                    'dayend':stringifyHourPlusMinute(dayEndH, dayEndM),
                    'max_temp': maxTemp,
                    'min_temp': minTemp,
                    'crit_concentartion': critConcentration,
                    'watering_duration': wateringDuration,
                    'success': True,
                    'min_lumen':minLumen,
                    'max_lumen':maxLumen,
                    'twitter_delay':twitterDelay.value
                    })

@app.route('/set_settings', methods=['POST'])
def setSettingsA():
    if not request.json:
        return jsonify({'res':'not a json'})
        global maxTemp
    global minTemp
    nt = minTemp
    xt = maxTemp
    if 'max_temp' in request.json:
        if (not(request.json['max_temp'] is None)):
           xt = int(request.json['max_temp'])
    if 'min_temp' in request.json:
        if (not(request.json['min_temp'] is None)):
           nt = int(request.json['min_temp'])
    if nt>=xt:
        return jsonify({'success':False, "reason":"minValue cannot be greater than maxValue"}),200
  
    global maxLumen
    global minLumen
    xl = maxLumen
    nl = minLumen
    if 'max_lumen' in request.json:
        if (not(request.json['max_lumen'] is None)):
           xl = int(request.json['max_lumen'])
    if 'min_lumen' in request.json:
        if (not(request.json['min_lumen'] is None)):
           nl = int(request.json['min_lumen'])
    if nl>=xl:
        return jsonify({'success':False, "reason":"minValue cannot be greater than maxValue"}),200

    if 'crit_concentration' in request.json:
        global critConcentration
        if (not(request.json['crit_concentration'] is None)):
           critConcentration = int(request.json['crit_concentration'])
    if 'watering_duration' in  request.json:
        global wateringDuration
        if (not(request.json['watering_duration'] is None)):
           wateringDuration = int(request.json['watering_duration'])
    if 'twitter_delay' in  request.json:
        global twitterDelay    
        if (not(request.json['twitter_delay'] is None)):
           twitterDelay.value = int(request.json['twitter_delay'])
    if 'daystart' in request.json:
        global dayStartH
        global dayStartM
        if (not(request.json['daystart'] is None)):
            d = datetime.datetime.strptime(request.json['daystart'],"%H:%M")
            dayStartH = d.hour
            dayStartM = d.minute
    if 'dayend' in request.json:
        global dayEndH
        global dayEndM
        if (not(request.json['dayend'] is None)):
            d = datetime.datetime.strptime(request.json['dayend'],"%H:%M")
            dayEndH = d.hour
            dayEndM = d.minute

    minTemp = nt
    maxTemp = xt
    maxLumen = xl
    minLumen = nl
    return jsonify({"succes":True})

def stringifyHourPlusMinute(hour, minute):
    h = str(hour);
    if (hour<10):
        h="0"+h
    m = str (minute);
    if (minute<10):
        m = "0"+m;
    return h+":"+m;
        

def str2bool(v):
    return v in ("yes", "true", "t", 1, True, "TRUE", "True", "Yes")

@app.route('/play', methods=['POST'])
def play():
    if not request.json:
        return jsonify({'res':'not a json'})
    global playing
    global buzzer
    playing = str2bool(request.json['play'])
    if (playing==False):
        buzzer.stopSound()
    return jsonify({'success':True}),200

@app.route('/set_max_temp', methods=['POST'])
def setMaxTempA():
    if not request.json:
        return jsonify({'res':'not a json'})
    global maxTemp
    global minTemp
    temp = request.json['value']
    if (minTemp>=temp):
        return jsonify({'success':False, "reason":"minValue cannot be greater than maxValue"}),200
    maxTemp = temp
    return jsonify({'success':True}),200


@app.route('/set_min_temp', methods=['POST'])
def setMinTempA():
    if not request.json:
        return jsonify({'res':'not a json'})
    global minTemp
    global maxTemp
    temp = request.json['value']
    if (maxTemp<=temp):
        return jsonify({'success':False, "reason":"minValue cannot be greater than maxValue"}),200
    minTemp = temp
    return jsonify({'success':True}), 200


@app.route('/display', methods=['POST'])
def postDisplay():
    #if not request.json or not 'sentense' in request.json:
    #    abort(400)
    global msg
    if not request.json:
        return jsonify({'res':'not a json'}), 200
    if not 'sentense' in request.json:
        return jsonify({'res':'not a sentense'}), 200

    lcd.clear()
    lcd.setCursor(0,0)
    lcd.setColor(255, 255, 255)
    msg = "" + request.json['sentense']
    lcd.write(msg.encode('utf8'))
    #time.sleep(900)
    return jsonify({'sentense':request.json['sentense'],
                    'success':True}), 200

@app.route('/delay', methods=['POST'])
def setTweetDelay():
    if not request.json:
        abort(400)
    if not request.json:
        return jsonify({'res':'not a json'}), 200
    twitterDelay.value = int(request.json['delay'])
    return jsonify({'success':True}), 200



@app.route('/leds', methods=['POST'])
def postLeds():
    if not request.json:
        abort(400)
    if not request.json:
        return jsonify({'res':'not a json'}), 200
    r = int(request.json['red'])
  
    b = int(request.json['blue'])
   
    y = int(request.json['yellow'])

    servo.write(y*90)
    
    blue.write(b)
    yellow.write(y)
    red.write(r)
    return jsonify({'success':True}), 200

def playSound():
    global melodyIndex
    while(True):
        if playing:
            print(melodyIndex)
            print(melody[melodyIndex])
            buzzer.playSound(melody[melodyIndex], 5000)            
            melodyIndex = (melodyIndex + 1) % (len(melody))
            time.sleep(2)


def updateStatus():
    global red
    global yellow
    global maxTemp
    global minTemp
    global api
    global windowBlocked
    global wateringBlocked
    global wateringDuration
    global lightingBlocked
    global minLumen
    global maxLumen
    while (True):
        if (time.time()>=wateringBlocked and d.value >= critConcentration):
            startWatering(wateringDuration)
            #tweetMessage(api, "Start watering!..")
        if isNight():
            lightingBlocked = 0;
            red.write(0)
        elif time.time()>lightingBlocked and t.value!=-1:
            if l.value>=maxLumen and red.read()!=1:
                print("RED ON")
                print(red.read())
                red.write(1)
                #tweetMessage(api, "Turn on the light!")
            if l.value<=minLumen and red.read()!=0:
                print("RED OFF")
                print(red.read())
                red.write(0)
                #tweetMessage(api, "Turn off the light...")
        if (time.time()<windowBlocked):
            continue
        if t.value != -1:
            if t.value>=maxTemp and servo.read()!=90:
                print("SERVO ON")
                tmp =yellow.read()
                print(tmp)
                servo.write(90)
                yellow.write(1)
                #tweetMessage(api, "open the door!")
            if t.value<=minTemp and servo.read()!=0:
                print("SERVO OFF")
                print(yellow.read())
                yellow.write(0)
                servo.write(0)
                #tweetMessage(api, "close the door!")
        time.sleep(1)


def tweet(t, l, d, delay):
    log = 1;
    consumer_key = "JKu67ffsWoJXO7PdsSyvzW2uX"
    consumer_secret="7bzJorfweKB6Hsbgl6fBuVCGDkpzfzhUfZMYLfRv5hKKXVvnZA"

    access_token="840962383610839041-WL6xSoREpAeRF4JkxFnCCsPLmjaalmV"
    access_token_secret="mqUhqsjHIBPgyPYQYVgIWxne521ZydS5tTcuqOO9B2uzg"

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

   
    while True:
        dl = delay.value
        print("TIME")
        print(time.time())
        print("tweeting...")
        message="temperature: "+str(t.value)+"\nlight: "+str(l.value)+"\ndust: "+str(d.value)
        tweetMessage(api, message)
        log=log+1
        time.sleep(dl)


def tweetMessage(api, message):
    api.update_status(status="Log " + str(time.time())+ ":\n"+message)

def getDust(v):
    startTime = time.time()
    while True:
        print('sleeping')
        data = dustSensor.getData()
        time.sleep(.1)
        print("Low pulse occupancy: " + str(data.lowPulseOccupancy))
        print("Ratio: " + str(data.ratio))
        print("Concentration: "+ str(data.concentration))
        print("wake up")

        currentTime = time.time()
        v.value = data.concentration


def getTemperature(t):
    while True:
        t.value = tempSensor.value()
        print("Temperature " + str(t.value))
        time.sleep(10)
def getLight(l):
    while True:
        l.value = lightSensor.value()
        print("Light " + str(l.value))
        time.sleep(10)

@app.route ("/open_window", methods=["POST"])
def openWindowA():
    if not request.json:
        abort(400)
    delay = request.json["delay"]
    thread = threading.Thread(target=openWindow,args=(int(delay),))
    thread.start()
    return jsonify({"success":True}), 200
   
@app.route ("/close_window", methods=["POST"])
def closeWindowA():
    global windowBlocked
    servo.write(0)
    yellow.write(0)
    windowBlocked = 0
    return jsonify({"success":True}), 200


@app.route ("/start_watering", methods=["POST"])
def startWateringA():
    if not request.json:
        abort(400)
    delay = request.json["delay"]
    d1 = int(delay)
    thread = threading.Thread(target=startWatering,args=(d1,))
    thread.start()
    return jsonify({"success":True}), 200
   
@app.route ("/stop_watering", methods=["POST"])
def stopWateringA():
    blue.write(0)
    global wateringBlocked
    wateringBlocked = 0
    return jsonify({"success":True}), 200

@app.route("/start_lighting", methods=["POST"])
def startLightingA():
    if not request.json:
        abort(400)
    delay = request.json["delay"]
    d1 = int(delay)
    thread = threading.Thread(target=startLighting,args=(d1,))
    thread.start()
    return jsonify({"success":True}), 200
   
@app.route ("/stop_lighting", methods=["POST"])
def stopLightingA():
    red.write(0)
    global lightingBlocked
    lightingBlocked = 0
    return jsonify({"success":True}), 200

def openWindow(delay):
    global windowBlocked
    servo.write(90)
    yellow.write(1)
    windowBlocked = time.time()+delay
    time.sleep(delay)
    if (time.time()>windowBlocked and windowBlocked!=0):
        servo.write(0)
        yellow.write(0)
        wateringBlocked = 0

def startWatering(delay):
    global wateringBlocked
    blue.write(1)
    wateringBlocked = time.time()+delay
    time.sleep(delay)
    if (time.time()>wateringBlocked and wateringBlocked!=0):
        blue.write(0)
        wateringBlocked = 0
 
def startLighting(delay):
    global lightingBlocked
    red.write(1)
    lightingBlocked = time.time()+delay
    time.sleep(delay)
    if (time.time()>lightingBlocked and lightingBlocked!=0):
        red.write(0)
        lightingBlocked = 0
              
def isNight():
    global dayStartH;
    global dayStartM;
    global dayEndH;
    global dayEndM;
    t = datetime.datetime.utcnow()
    return t.hour<dayStartH or t.hour>dayEndH or t.hour==dayStartH and t.minute==dayStartM or t.hour==dayEndH and t.minute==dayEndM




if __name__== '__main__':
    app.run(debug=True)

windowBlocked = 0;
wateringBlocked = 0;
lightingBlocked = 0;

consumer_key = "JKu67ffsWoJXO7PdsSyvzW2uX"
consumer_secret="7bzJorfweKB6Hsbgl6fBuVCGDkpzfzhUfZMYLfRv5hKKXVvnZA"

access_token="840962383610839041-WL6xSoREpAeRF4JkxFnCCsPLmjaalmV"
access_token_secret="mqUhqsjHIBPgyPYQYVgIWxne521ZydS5tTcuqOO9B2uzg"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

lcd = Lcd.Jhd1313m1(6, 0x3E, 0x62)
red=mraa.Gpio(4)
red.dir(mraa.DIR_OUT)
blue=mraa.Gpio(2)
blue.dir(mraa.DIR_OUT)
yellow=mraa.Gpio(3)
yellow.dir(mraa.DIR_OUT)

buzzer = upmBuzzer.Buzzer(5)
playing = False;
buzzer.stopSound()

msg = ""
la = upmBuzzer.LA
sol = upmBuzzer.SOL
mi = upmBuzzer.MI
melody = (la, mi, la, mi, la, sol, sol)
melodyIndex = 0;
t1 = threading.Thread(target=playSound)
t1.start()

dustSensor = upmPpd.PPD42NS(8)
tempSensor = grove.GroveTemp(1)
lightSensor = grove.GroveLight(3)

servo = Servo.Servo("Window")
servo.attach(3)
servo.write(0)

d = Value('d', -1 )
t = Value('d', -1 )
l = Value('d', -1 )
twitterDelay= Value('d',10)
dustProcess = Process(target=getDust, args=(d,))
lightProcess = Process(target = getLight, args=(l,))
tempProcess = Process(target = getTemperature, args=(t,))
tweetProcess = Process(target=tweet, args=(t, l, d, twitterDelay))
dustProcess.start()
lightProcess.start()
tempProcess.start()
tweetProcess.start()

maxTemp = 15
minTemp = 12

critConcentration = 500
wateringDuration = 300

minLumen = 3;
maxLumen = 6;
dayStartH = 8;
dayStartM = 0;
dayEndH = 23;
dayEndM = 20;

statusThread = threading.Thread(target = updateStatus)
statusThread.start()
